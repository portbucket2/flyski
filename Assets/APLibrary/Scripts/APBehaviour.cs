﻿using UnityEngine;
using com.alphapotato.utility;

[DefaultExecutionOrder(ConstantManager.APBehaviourOrder)]
public class APBehaviour : MonoBehaviour
{
    [HideInInspector]
    public APManager gameManager;
    [HideInInspector]
    public APTools APTools;

    [HideInInspector]
    public GameplayData gameplayData;
    //[HideInInspector]
    public GameState gameState;

    public virtual void Awake()
    {
        APTools = APTools.Instance;
    }

    public virtual void Start()
    {
    }

    public virtual void OnEnable()
    {
        APManager.OnSelfDistributionAction += OnSelfDistributionAction;
        APManager.OnNone += OnNone;
        APManager.OnIdle += OnIdle;
        APManager.OnGameInitializing += OnGameInitializing;
        APManager.OnGameStart += OnGameStart;
        APManager.OnGameOver += OnGameOver;
        APManager.OnChangeGameState += OnChangeGameState;
        APManager.OnCompleteTask += OnCompleteTask;

    }

    public virtual void OnDisable()
    {
        APManager.OnSelfDistributionAction -= OnSelfDistributionAction;
        APManager.OnNone -= OnNone;
        APManager.OnIdle -= OnIdle;
        APManager.OnGameInitializing -= OnGameInitializing;
        APManager.OnGameStart -= OnGameStart;
        APManager.OnGameOver -= OnGameOver;
        APManager.OnChangeGameState -= OnChangeGameState;
        APManager.OnCompleteTask -= OnCompleteTask;

    }

    public virtual void OnSelfDistributionAction(APManager aPManager)
    {
        gameManager = aPManager;
        gameplayData = aPManager.gameplayData;
    }

    public virtual void OnChangeGameState(GameState gameState)
    {
        this.gameState = gameState;
    }

    public virtual void OnNone()
    {
    }

    public virtual void OnIdle()
    {
    }

    public virtual void OnGameInitializing()
    {        
    }

    public virtual void OnGameStart()
    {        
    }

    public virtual void OnGameOver()
    {
    }

    public virtual void OnCompleteTask()
    {
    }

}
