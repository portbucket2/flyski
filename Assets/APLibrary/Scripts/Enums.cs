﻿public enum LoadLevelType {

    LOAD_BY_NAME,
    LOAD_BY_INDEX
}

public enum GameState
{
    NONE, // Game wii start loading gameplay data in this state, this state will call once game opend
    IDLE, // After loading gameplay data this state will be called, Game will be ready to play
    GAME_INITIALIZED,
    GAME_PLAY_STARTED,
    GAME_PLAY_ENDED
}

public enum Powerups
{
    NONE,
    SIZE_UP,
    MAGNET,
    SPEED_UP
}

public enum EnvironmentsType
{
    CONCREAT,
    DIRT,
    GREEN
}