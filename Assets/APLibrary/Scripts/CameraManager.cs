﻿/*
 * Developer E-mail: sandsoftimer@gmail.com
 * Facebook Account: https://www.facebook.com/md.imran.hossain.902
 * This is a manager which will give common functional supports. 
 * like, Camera shaking, Slowmotioning, Vector from screen touch etc.
 *  
 */

using System.Collections;
using UnityEngine;

namespace com.alphapotato.utility
{

    public class CameraManager : MonoBehaviour
    {

        public void ShakeDefaultCamera(float duration = 0.04f, float magnitude = 0.02f, bool isCameraLocal = false)
        {
            StartCoroutine(ShakeDefaultCam(duration, magnitude, isCameraLocal));
        }

        IEnumerator ShakeDefaultCam(float duration, float magnitude, bool isCameraLocal = false)
        {

            Transform cam = Camera.main.transform;

            if (isCameraLocal)
            {
                Vector3 initialPosition = cam.localPosition;

                float elapseTime = 0.0f;

                while (elapseTime <= duration)
                {

                    elapseTime += Time.deltaTime;

                    Vector3 noise = new Vector3(
                        Random.Range(-1f, 1f) * magnitude,
                        Random.Range(-1f, 1f) * magnitude,
                        Random.Range(-1f, 1f) * magnitude
                        );

                    noise.x += initialPosition.x;
                    noise.y += initialPosition.y;
                    noise.z += initialPosition.z;

                    cam.transform.localPosition = noise;

                    yield return null;
                }

                cam.localPosition = initialPosition;

            }
            else {

                Vector3 initialPosition = cam.position;

                float elapseTime = 0.0f;

                while (elapseTime <= duration)
                {

                    elapseTime += Time.deltaTime;

                    Vector3 noise = new Vector3(
                        Random.Range(-1f, 1f) * magnitude,
                        Random.Range(-1f, 1f) * magnitude,
                        Random.Range(-1f, 1f) * magnitude
                        );

                    noise.x += initialPosition.x;
                    noise.y += initialPosition.y;
                    noise.z += initialPosition.z;

                    cam.transform.position = noise;

                    yield return null;
                }

                cam.position = initialPosition;

            }
        }


        bool isSlowMotionActive = false;
        float slowMotionDuration;
        public void DoSlowMotion(float duration = 2f, float magnitude = 0.05f)
        {

            if (isSlowMotionActive)
                return;

            slowMotionDuration = duration;
            Time.timeScale = magnitude;
            Time.fixedDeltaTime = Time.timeScale * 0.2f;
            isSlowMotionActive = true;
        }

        void UpdateSlowMotion()
        {

            if (isSlowMotionActive)
            {

                Time.timeScale += (1 / slowMotionDuration) * Time.unscaledDeltaTime;

                if (Time.timeScale > 1)
                {

                    Time.timeScale = 1;
                    isSlowMotionActive = false;
                }
            }
        }

        private void Update()
        {
            UpdateSlowMotion();
        }

        public Vector3 GetWorldTouchPosition(Vector3 mouseTouchPoint)
        {
            Plane plane = new Plane(Vector3.up, 0);

            float distance;
            Ray ray = Camera.main.ScreenPointToRay(mouseTouchPoint);
            if (plane.Raycast(ray, out distance))
            {
                return ray.GetPoint(distance);
                
            }

            return Vector3.zero;
        }
    }
}
