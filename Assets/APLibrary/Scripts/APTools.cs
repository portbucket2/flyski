﻿/*
 * Developer E-mail: sandsoftimer@gmail.com
 * Facebook Account: https://www.facebook.com/md.imran.hossain.902
 * 
 * Features:
 * Scene Loading Effect
 * GameData Save/Load
 * Camera Effects (Slow Mosition, Shake Camera)
 * Objects Pulling/Pushing Effect
 */

using System.IO;
using UnityEditor;
using UnityEngine;

namespace com.alphapotato.utility
{
    [DefaultExecutionOrder(ConstantManager.APToolOrder)]
    public class APTools : MonoBehaviour
    {
        public static APTools Instance;

        public SceneManager sceneManager;
        public SavefileManager savefileManager;
        public CameraManager cameraManager;
        public PoolManager poolManager;
        public MathManager mathManager;

        #region Singleton Pattern
        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        #endregion  Singleton Pattern


#if UNITY_EDITOR
        [MenuItem("APTools/Delete Gameplay Data")]
        public static void DeleteBinaryData()
        {
            PlayerPrefs.DeleteAll();
        }
#endif
    }


}