﻿#if UNITY_EDITOR
using System.Linq;
using UnityEditor;
using UnityEngine;

[System.Obsolete]
public class APConfigarator : AssetPostprocessor
{
    static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
    {
        #region Debug log for anything happend on asset folder
        //foreach (string str in importedAssets)
        //{
        //    Debug.Log("Reimported Asset: " + str);
        //}
        //foreach (string str in deletedAssets)
        //{
        //    Debug.Log("Deleted Asset: " + str);
        //}

        //for (int i = 0; i < movedAssets.Length; i++)
        //{
        //    Debug.Log("Moved Asset: " + movedAssets[i] + " from: " + movedFromAssetPaths[i]);
        //}
        #endregion Debug log for anything happend on asset folder

        #region Setup Unity Layers & Tags
        SerializedObject manager = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0]);
        SerializedProperty layersProp = manager.FindProperty("layers");
        string[] layersName = ConstantManager.layerNames.Values.ToArray();

        bool layerMismatchfound = false;
        for (int i = 0; i < layersName.Length; i++)
        {
            SerializedProperty sp = layersProp.GetArrayElementAtIndex(i);
            if (!layersName[i].Equals(sp.stringValue))
            {
                layerMismatchfound = true;
                Debug.LogError("Unity Layer Names has been <Color=red>Repaired</Color>. To modify/add layers, see layerNames in <Color=Yellow>ConstantManager.cs</Color>");
                break;
            }
        }

        if (layerMismatchfound)
        {
            for (int i = 8; i < layersName.Length; i++)
            {
                SerializedProperty layerSP = layersProp.GetArrayElementAtIndex(i);
                layerSP.stringValue = layersName[i];
                manager.ApplyModifiedProperties();
            }
        }
    }

    #endregion Setup Unity Layers
}
#endif