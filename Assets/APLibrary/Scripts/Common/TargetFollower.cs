﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetFollower : MonoBehaviour
{
    public Transform followTarget, lookTarget;

    [Header("Follow Properties")]
    public bool follow;
    [Range(0f, 1f)]
    public float followSmoothness;
    public Vector3 followOffset;

    [Header("Look Properties")]
    public bool lookAtTarget;
    [Range(0f, 1f)]
    public float lookSmoothness;
    public Vector3 lookOffset;

    Vector3 velocity;
    private void FixedUpdate()
    {
        if (lookAtTarget)
        {
            var targetRotation = Quaternion.LookRotation(lookTarget.transform.position + lookOffset - transform.position);
            transform.rotation = Quaternion.Slerp(targetRotation, transform.rotation, lookSmoothness);
        }

        if (follow)
        {
            transform.position = Vector3.SmoothDamp(transform.position, followTarget.position + followOffset, ref velocity, followSmoothness);
        }

    }
}
