﻿using System;
using UnityEngine;
using com.alphapotato.utility;
using System.Collections;

[DefaultExecutionOrder(ConstantManager.APManagerOrder)]
public class APManager : MonoBehaviour
{
    public static Action<APManager> OnSelfDistributionAction;
    public static event Action<GameState> OnChangeGameState;
    public static event Action OnNone;
    public static event Action OnIdle;
    public static event Action OnGameInitializing;
    public static event Action OnGameStart;
    public static event Action OnGameOver;
    public static event Action OnCompleteTask;

    public GameplayData gameplayData = new GameplayData();
    public GameState gameState;
    public ParticleSystem gameOverEffect;
    public Animator gamePlayUI, gameSuccessUI, gameFaildUI;

    public int totalGivenTask = 0, totalCompletedTask = 0;
    [HideInInspector]
    public APTools APTools;

    public virtual void Awake()
    {
        APTools = APTools.Instance;
    }

    public virtual void Start()
    {

    }

    public virtual void OnCompleteATask()
    {
        totalCompletedTask++;
        CompleteATask();

        if (totalCompletedTask.Equals(totalGivenTask))
        {
            gameplayData.isGameoverSuccess = true;
            gameplayData.gameEndTime = Time.time;

            ChangeGameState(GameState.GAME_PLAY_ENDED);
        }
    }

    public int GetModedLevelNumber()
    {
        return gameplayData.currentLevelNumber % ConstantManager.TOTAL_GAME_LEVELS;
    }

    public int GetLevelMultiplayer()
    {
        return gameplayData.currentLevelNumber / ConstantManager.TOTAL_GAME_LEVELS;
    }

    public virtual void NextLevel()
    {
        APTools.sceneManager.LoadNextLevel();
    }

    public virtual void ChangeGameState(GameState gameState)
    {
        this.gameState = gameState;
        StartCoroutine(ChangeState(gameState));
    }

    IEnumerator ChangeState(GameState gameState)
    {
        yield return null;
        Debug.Log("Executing: " + gameState.ToString());
        OnChangeGameState?.Invoke(gameState);

        switch (gameState)
        {
            case GameState.NONE:
                None();
                break;
            case GameState.IDLE:
                Idle();
                break;
            case GameState.GAME_INITIALIZED:
                GameInitializing();
                break;
            case GameState.GAME_PLAY_STARTED:
                GameStart();
                break;
            case GameState.GAME_PLAY_ENDED:
                GameOver();
                break;
        }
    }

    public virtual void None()
    {
        OnNone?.Invoke();
    }

    public virtual void Idle()
    {
        OnSelfDistributionAction?.Invoke(this);
        OnIdle?.Invoke();
    }

    public virtual void GameInitializing()
    {
        OnGameInitializing?.Invoke();
    }

    public virtual void GameStart()
    {
        OnGameStart?.Invoke();
    }

    public virtual void GameOver()
    {
        OnGameOver?.Invoke();        
    }

    public virtual void CompleteATask()
    {
        OnCompleteTask?.Invoke();
    }


}
