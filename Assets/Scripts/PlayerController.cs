﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : APBehaviour
{
    public Transform groundCheckerPoint, parasuitObj;
    public SkiPhysicsData skiPhysicsData;
    public TargetFollower cameraFollower;
    public Vector3 centerOfMass;
    public float inputDragThreshold = 1f, groundCheckDistance = 1f;
    public bool isGrounded;


    Rigidbody rig;
    Animator anim;
    bool alive;
    float horizontalForce, forwardForce;

    Vector3 lastMousePosition;
    Vector3 velocity;

    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();

        rig = GetComponent<Rigidbody>();
        rig.centerOfMass = centerOfMass;
        anim = GetComponent<Animator>();
        lastMousePosition = Vector3.zero;

        Physics.gravity = skiPhysicsData.gravity;
        rig.mass = skiPhysicsData.mass;
        rig.drag = skiPhysicsData.linearDrag;
        horizontalForce = skiPhysicsData.horizontalForce;
        forwardForce = skiPhysicsData.forwardForce;
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    private void Update()
    {
        isGrounded = Physics.Raycast(new Ray(groundCheckerPoint.position, -Vector3.up), groundCheckDistance, 1 << ConstantManager.GROUND_LAYER);
        parasuitObj.gameObject.SetActive(!isGrounded);


        if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        {
            gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
            cameraFollower.follow = true;
            cameraFollower.lookAtTarget = true;
        }

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

        if(Input.GetMouseButtonDown(0))
            lastMousePosition = Input.mousePosition;

        velocity = transform.forward;
        //velocity.y = rig.velocity.y;
        velocity.z = forwardForce;

        if (Input.GetMouseButton(0))
        {
            Vector3 inputDelta = Input.mousePosition - lastMousePosition;
            if ( Mathf.Abs(inputDelta.x) >= inputDragThreshold)
            {
                //velocity.x = Mathf.Abs(rig.velocity.x) * (inputDelta.x >= 0 ? 1 : -1);

                velocity.x = horizontalForce * (inputDelta.x >= 0 ? 1 : -1);
                //transform.position += new Vector3(horizontalSpeed * Time.deltaTime * (inputDelta.x > 1 ? 1 : -1), 0, 0);
                Debug.LogError("Moving Value: " + velocity.x);
            }
        }
        if(Input.GetMouseButtonUp(0))
            velocity.x = 0;

        //rig.velocity = velocity;

    }

    private void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED) || !isGrounded)
            return;

        float h = velocity.x;
        velocity = transform.forward * forwardForce * Time.fixedDeltaTime;
        velocity.x = h;
        //velocity.y = -Vector3.up.y;
        rig.AddForce(velocity);
    }

    private void LateUpdate()
    {
        lastMousePosition = Input.mousePosition;
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS


    #endregion ALL SELF DECLEAR FUNCTIONS

}
