﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : APManager
{

    public override void Awake()
    {
        base.Awake();

        ChangeGameState(GameState.IDLE);
    }

    public override void Idle()
    {
        base.Idle();
        gameplayData = APTools.savefileManager.LoadGameData();


        ChangeGameState(GameState.GAME_INITIALIZED);
    }

    
}
