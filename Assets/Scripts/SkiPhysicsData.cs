﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SkyPhysicsData", menuName = "SkiPhysicsData")]
public class SkiPhysicsData : ScriptableObject
{
    public Vector3 gravity = new Vector3(0, -9.81f, 0);
    public float mass = 5f;
    public float linearDrag = 0.3f;
    public float angularDrag = 3f;
    public float horizontalForce = 50f;
    public float forwardForce = 7000f;


}
